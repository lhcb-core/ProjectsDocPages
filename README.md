Releasing LHCb Documentation
============================

To simplify creation of project sites two scripts was created:

* **addrel.py** to add version to project.

    Usage:
    ```
    python addrel.py projectname projectversion [branch]
    ```

   If project with such name or version for given project name does not exist the error message will be displayed. First two parameters are mandatory. The third parameter is used to create branch (i.e. DC06, DC04, RTTC are branches)

* **remrel.py** to remove version from project.

    Usage:
    ```
    python remrel.py projectname projectversion [branch]
    ```

    If third parameter is given only version from branch will be removed.


Theese scripts have to be started from directory wich contain project directories. Scripts search project directories in `$LHCBDOC` (= `/eos/project/l/lhcbwebsites/www/projects`).
Parameter `projectname` have to be exactly the same as name of project directory in `$LHCBDOC` directory.

---

$LHCBDOC directory structure:

    php_scripts (this project)
    scripts -> php_scripts/scripts
        addrel.py
        remrel.py
        gc_archive.py
    project
        scripts -> ../php_scripts
        css
            css.css
        images
            lhcblogo.gif
            other images
        releases
            index.php -> ../scripts/gc_releases.php
            latest -> vXrY (latest version)
            vXrY (one dir per version)
                index.php -> ../../scripts/release.php
                release.notes.php -> ../../scripts/release.notes_wrapper.php
                requirements.php -> ../../scripts/requirements_wrapper.php
                release.notes -> ../../projectroot/PROJECT_vXrY/ProjectSys/doc/release.notes
                requirements -> ../../projectroot/PROJECT_vXrY/ProjectSys/cmt/requirements
                description.html (optional description of the release)
            "branch" (one per branch, e.g. data type)
                index.php -> ../../scripts/releases.php
                latest -> vXrY
                vXrY -> ../vXrY (one per relevant version)
            archive
                index.php -> ../../scripts/gc_releases.php
                vXrY (one per archived version)
        packages
            index.php -> ../scripts/packages.php
            package.php -> ../scripts/package.php
            Hat/PackageName (one directory per package, obsolete)
        maindesc.html (project description)
        projectroot -> /cvmfs/lchb.cern.ch/lib/lhcb/PROJECT
        latest -> releases/latest
        index.php -> scripts/main.php
        release.notes -> latest/release.notes


## To add a new project:

* Create directory with the **name** of project (*projectdir*).
* Copy content of directory **template** to *projectdir*.
* Edit file *maindesc.html* in *projectdir*, content of this file will be displayed on main page.

## To add a version to a project:

* With the help of script addrel.py create versions and branches.
  It should be started from directory wich contain projectdir directory.
* Create file *description.html* in directories *projectdir/releases/versionnumber*,
  information in this file will be displayed in the table with information about releases.
* Format of file *desctiption.html*:

  ```.html
  <td id="obsolete|production"><p>content<p><td>
  ```

## Examples:
```.sh
cd $LHCBDOC/scripts
cp -r $LHCBDOC/php_scripts/template online
nedit online/maindesc.html
./addrel.py online v1r4
./addrel.py online v2r0
./addrel.py online v1r5
nedit online/releases/v1r4/description.html
nedit online/releases/v1r5/description.html
nedit online/releases/v2r0/description.html
```

```.sh
cd $LHCBDOC/scripts
cp -r $LHCBDOC/php_scripts/template panoramix
nedit panoramix/maindesc.html
./addrel.py panoramix v13r10 DC06
./addrel.py panoramix v14r0
./addrel.py panoramix v9r8 DC04
nedit panoramix/releases/v13r10/description.html
nedit panoramix/releases/v14r0/description.html
nedit panoramix/releases/v9r8/description.html
```

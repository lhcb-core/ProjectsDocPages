<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <?php include(realpath(dirname(__FILE__))."/style.php"); ?>

    <title>     
      <?php
        $dir = getcwd();
        $releasever = end(explode('/', $dir));
        echo $projectname . " " . $releasever;
      ?>
    </title>
  </head>

  <body>
    <?php include($scrpbase."/scripts/title.php"); ?>
    <?php include($scrpbase."/scripts/release_title.php"); ?>

    <div class=pagebody>

    <?php
      if(file_exists("description.html")) {
        $fl = file("description.html");
        echo "<h2>Description</h2>";
        foreach($fl as $linenum => $line) echo $line;
      }
    ?>

    <?php
      if (file_exists('release.notes')) {
        echo "<h2>Release Notes </h2>";
        $fl = file("release.notes");
        $linenum1 = 0;
        $linenum2 = 0;
        echo "<div id=requ><p>";
        //echo "<pre>";
        foreach($fl as $linenum => $line)
        {
          if($linenum1 != 0 && $linenum2 == 0 && (strstr($line, "<H1>") || strstr($line, "!="))) $linenum2 = $linenum;
          if($linenum1 != 0 && $linenum2 == 0) {
            $regs = array();
            if(ereg("^[ ]+", $line, $regs))
            {
              $cnt = strlen($regs[0]);
              for($i = 0; $i < $cnt; $i++) echo "&nbsp;";
            }
            
            echo ereg_replace("<[^>]+>[ ]*", "", ereg_replace("^[ ]+", "", $line))."<br>";
          }
          if($linenum1 == 0 && $linenum2 == 0 && (strstr($line, "<H1>") || strstr($line, "!=")) && strstr($line, $releasever)) $linenum1 = $linenum;
        }
        if($linenum1 == 0 && $linenum2 == 0) { foreach($fl as $line) { echo $line; } }
        //echo "</pre>";
        echo "</p></div>";
      }
    ?>

  <br>

  </div>

  <?php include($scrpbase."/scripts/release_links.php"); ?>
  <?php include($scrpbase."/scripts/links.php"); ?>

  </body>
</html>

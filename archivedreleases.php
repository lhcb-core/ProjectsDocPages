<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <?php include(realpath(dirname(__FILE__))."/style.php"); ?>
    <?php include(realpath(dirname(__FILE__))."/archstyle.php"); ?>
    <title><?php echo $projectname ?> Archived Releases</title>
  </head>

  <body>

  <?php include($scrpbase."/scripts/title.php"); ?>
    
  <div class=pagebody>

  <?php
    echo "<h2>$projectname Archived Releases</h2>";
    echo "<p> Here is a summary of the different versions of
    $projectname available. You can click on the links in the first
    column to access each individual release.</p>";

    include($scrpbase."/scripts/archlist.php"); ?>
    
    
    <br>

    <table>
    <?php
      foreach($releaselist_back as $rel) if(file_exists($rel)) {
        echo '<tr>';
        echo '<td class=firstcell>' ;
        echo "<a href=\"$rel\"> $rel </a>" ;
        echo '</td>';

        echo '<td align="center">' ;
	echo 'Released on ' ;
        if (file_exists("$rel/release.notes")) {
          echo date("Y-m-d",filemtime(realpath("$rel/release.notes"))); }
        else { echo "NA"; }
        echo '</td>';

        echo '<td align="center">' ;
	echo 'Archived on ' ;
        if (file_exists("$rel/requirements")) {
          echo date("Y-m-d",filemtime(realpath("$rel/requirements"))); }
        else { echo "NA"; }
        echo '</td>';

        echo '<td align="center">' ;
        if (file_exists("$rel/release.notes")) {
          echo "<a href=\"$rel/release.notes.php\">Release Notes</a>"; }
        else { echo "NA"; }
        echo '</td>';

        $descfile = $scrpbase . "/releases/archive/" . $rel . "/description.html";
        if(file_exists($descfile)) {
          $fl = file($descfile);
          foreach($fl as $linenum => $line) echo $line; }
        else { echo "<td align=\"center\" width=\"50%\">N/A</td>"; }

        echo '</tr>';
      }
    ?>
    </table>

  <br><br><br>

  </div>

  <?php include($scrpbase."/scripts/links.php"); ?>

  </body>
</html>

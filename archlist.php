<?php

    $reldir = "$scrpbase/releases/archive";

    $releaselist_back = array();
    $branchlist_back = array();
    $releaselist = array();
    $branchlist = array();
    $v = array();
    $r = array();
    $p = array();
    $i = 0;

    if(is_dir($reldir)) {
      if($dh = opendir($reldir)) {
        while(($file = readdir($dh)) !== false) {
          if(ereg("v[0-9]+(r[0-9]+(p[0-9]+)?)?", $file)) {
             $releaselist[$i] = $file;
             $releaselist_back[$i] = $file;
             $spl = preg_split("/[rvp]/", $releaselist[$i]);
             if(count($spl) > 1) { $v[$i] = $spl[1]; } else $v[$i] = 0;
             if(count($spl) > 2) { $r[$i] = $spl[2]; } else $r[$i] = 0;
             if(count($spl) > 3) { $p[$i] = $spl[3]; } else $p[$i] = 0;
             $i++;
           }
        }
      }
      closedir($dh) ;    
    }
    
    array_multisort($v, SORT_DESC, $r, SORT_DESC, $p, SORT_DESC, $releaselist_back, $releaselist);
    array_multisort($v, SORT_ASC, $r, SORT_ASC, $p, SORT_ASC, $releaselist);
    
?>

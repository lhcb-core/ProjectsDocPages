<?php
$DocsDB = json_decode(file_get_contents("https://lhcb-doxygen.web.cern.ch/lhcb-doxygen/docs/db.json"), true);

function doxygen_exists($project, $version) {
  global $DocsDB;
  return $DocsDB != NULL && array_key_exists(strtolower($project) . "/" . $version, $DocsDB);
}
function doxygen_url($project, $version) {
  return "https://lhcb-doxygen.web.cern.ch/lhcb-doxygen/" . strtolower($project) . "/" . $version . "/index.html";
}

?>

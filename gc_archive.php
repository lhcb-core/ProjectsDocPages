<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <?php include(realpath(dirname(__FILE__))."/style.php"); ?>

    <title><?php echo $projectname ?> Archived Versions</title>
  </head>

  <body>

  <?php include($scrpbase."/scripts/title.php"); ?>
    
  <div class=pagebody>

  <?php
    echo "<h2>$projectname Archived Versions</h2>";
    echo "<p> Here is a summary of officially release versions of
    $projectname that have been archived on CASTOR.</p>
    <p>Some development versions may have only been tagged in SVN (CVS for very old versions)
       and never deployed: as such they do not appear in this list.</p>";

    include($scrpbase."/scripts/gc_arlist.php"); ?>
    
    <table>
      <tr>
      <td class=firstcell> Version </td>
      <td> Release date </td>
      <td> Archive date </td>
      <td> Description </td>

    <?php
      foreach($archivelist_back as $arc) if(file_exists($arc)) {
        echo '<tr>';
        echo '<td class=firstcell>' ;
        echo "$arc" ;
        echo '</td>';

        echo '<td align="center">' ;
        $relfile = $scrpbase . "/releases/archive/" . $arc . "/reldate.txt";
        if(file_exists("$relfile")) {
          $fl = file($relfile);
	  foreach($fl as $linenum => $line) echo $line;
	}
        else { 
	  echo "NA";
	}
        echo '</td>';

        echo '<td align="center">' ;
        $arcfile = $scrpbase . "/releases/archive/" . $arc . "/arcdate.txt";
        if(file_exists("$arcfile")) {
          $fl = file($arcfile);
	  foreach($fl as $linenum => $line) echo $line;
	}
        else { 
	  echo "NA";
	}
        echo '</td>';

        $descfile = $scrpbase . "/releases/archive/" . $arc . "/description.html";
        if(file_exists($descfile)) {
          $fl = file($descfile);
          foreach($fl as $linenum => $line) echo $line; }
        else { echo "<td align=\"center\" width=\"50%\">N/A</td>"; }

        echo '</tr>';
      }
    ?>
    </table>

  <br><br><br>

  </div>

  <?php include($scrpbase."/scripts/links.php"); ?>

  </body>
</html>

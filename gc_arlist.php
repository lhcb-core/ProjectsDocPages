<?php

    $arcdir = "$scrpbase/releases/archive";

    $archivelist_back = array();
    $branchlist_back = array();
    $archivelist = array();
    $branchlist = array();
    $v = array();
    $r = array();
    $p = array();
    $i = 0;

    if(is_dir($arcdir)) {
      if($dh = opendir($arcdir)) {
        while(($file = readdir($dh)) !== false) {
          if(ereg("v[0-9]+(r[0-9]+(p[0-9]+)?)?", $file)) {
             $archivelist[$i] = $file;
             $archivelist_back[$i] = $file;
             $spl = preg_split("/[rvp]/", $archivelist[$i]);
             if(count($spl) > 1) { $v[$i] = $spl[1]; } else $v[$i] = 0;
             if(count($spl) > 2) { $r[$i] = $spl[2]; } else $r[$i] = 0;
             if(count($spl) > 3) { $p[$i] = $spl[3]; } else $p[$i] = 0;
             $i++;
           }
        }
      }
      closedir($dh) ;    
    }
    
    array_multisort($v, SORT_DESC, $r, SORT_DESC, $p, SORT_DESC, $archivelist_back, $archivelist);
    array_multisort($v, SORT_ASC, $r, SORT_ASC, $p, SORT_ASC, $archivelist);
    
    foreach($archivelist as $arc)
    {
      if(is_dir($arcdir)) {
        if ($dh = opendir($arcdir)) {
          while (($file = readdir($dh)) !== false) {
            if(!(array_search($file, array(".", "..")) > -1) && file_exists($arcdir . "/" . $file . "/" . $arc)) {
              $branchlist[$arc] = $file;
              break;
            }
            else { $branchlist[$arc] = ""; }
          }
        }
        closedir($dh);
      }
    }

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <?php include(realpath(dirname(__FILE__))."/style.php"); ?>
    
    <title><?php echo $projectname ?> Packages</title>
  </head>

  <body>
  <?php include($scrpbase."/scripts/title.php") ?>
  <div class=pagebody>

  <h2><?php echo $projectname ?> Packages</h2>
  <p> The following table gives you access to the release notes
      of the different packages. The first row relates to the steering
      package of the project and since it contains only the release
      notes and the requirements of the project, it points back to
      the corresponding release. </p> 

  <?php
    include($scrpbase."/scripts/pkglist.php");
    include($scrpbase."/scripts/relist.php");

    $nbofpkg = count($pkglist);
    $nbofrel = count($releaselist_back); ?>

  <table border=1>
    <tr>
      <td class=firstcell>
        <?php if(file_exists("$scrpbase/packages/$projectname/index.php")) {
          echo "<a href=\"$project_base/packages/$projectname\">$projectname</a>"; }
        else { echo "$projectname"; } ?>
      </td>

      <?php for($j = 0; $j < $nbofrel; $j++) {
        $ver = $releaselist_back[$j];
        echo '<td>' ;
        echo "<a href=\"$project_base/releases/$ver\"> $ver </a>" ;
        echo '</td>' ;
      } ?>
    </tr>

    <?php for($i = 1; $i < $nbofpkg; $i++) {

      $pkg = $pkglist[$i];

      $pkgexist = 0;
      for($j = 0; $j < $nbofrel; $j++) {
        $releasever = $releaselist_back[$j];
        $pkgdir = $projectdirprefix . $releasever . "/" . $pkg;
        $pkgver = "";

        if(is_dir($pkgdir) && $dh = opendir($pkgdir)) { while(($file = readdir($dh)) !== false) {
          if(!(array_search($file,$ignoredDirectory) > -1)) $pkgver = $file; }
        closedir($dh); }
        
        if($pkgver != "") $pkgexist = 1;
      }
      
      if($pkgexist != 0)
      {
        echo "<tr>";
        echo "<td class=firstcell>";
        if(file_exists("$scrpbase/packages/$pkg/index.php")) {
          echo "<a href=\"$project_base/packages/$pkg\">$pkg</a>"; }
        else { echo "$pkg"; }
        echo "</td>" ;

        for($j = 0; $j < $nbofrel; $j++) {
          $releasever = $releaselist_back[$j];
          $pkgdir = $projectdirprefix . $releasever . "/" . $pkg;
          $pkgver = "";
	  $pkgvercmtf = $projectdirprefix . $releasever . "/" . $pkg . "/cmt/version.cmt" ;
	  if(file_exists($pkgvercmtf)){
	    $pkgver = implode('',file($pkgvercmtf)) ;
	  } else {
          if(is_dir($pkgdir) && $dh = opendir($pkgdir)) { while(($file = readdir($dh)) !== false) {
            if(!(array_search($file,$ignoredDirectory) > -1)) $pkgver = $file; }
          closedir($dh); }
	  }
          echo "<td><a href=\"$project_base/packages/package.php?relver=" . $releasever . "&pkgname=" . $pkg . "&pkgver=" . $pkgver . "\">" . $pkgver . "</a></td>";
        }
      }
    } ?>
    </tr>
  </table>

  <h2>SVN repostories of the different packages</h2>
  <table border=1>

    <?php for($i = 1; $i < $nbofpkg; $i++) {

      $pkg = $pkglist[$i];

      $pkgexist = 0;
      for($j = 0; $j < $nbofrel; $j++) {
        $releasever = $releaselist_back[$j];
        $pkgdir = $projectdirprefix . $releasever . "/" . $pkg;
        $pkgver = "";

        if(is_dir($pkgdir) && $dh = opendir($pkgdir)) { while(($file = readdir($dh)) !== false) {
          if(!(array_search($file,$ignoredDirectory) > -1)) $pkgver = $file; }
        closedir($dh); }
        
        if($pkgver != "") $pkgexist = 1;
      }
      
      if($pkgexist != 0)
      {
        echo "<tr>";
        echo "<td class=firstcell>";
        if(file_exists("$scrpbase/packages/$pkg/index.php")) {
          echo "<a href=\"$project_base/packages/$pkg\">$pkg</a>"; }
        else { echo "$pkg"; }
        echo "</td>" ;

        for($j = 0; $j < $nbofrel; $j++) {
          $releasever = $releaselist_back[$j];
          $pkgdir = $projectdirprefix . $releasever . "/" . $pkg;
          $pkgver = "";
	  $pkgvercmtf = $projectdirprefix . $releasever . "/" . $pkg . "/cmt/version.cmt" ;
	  if(file_exists($pkgvercmtf)){
	    $pkgver = implode('',file($pkgvercmtf)) ;
	  } else {
          if(is_dir($pkgdir) && $dh = opendir($pkgdir)) { while(($file = readdir($dh)) !== false) {
            if(!(array_search($file,$ignoredDirectory) > -1)) $pkgver = $file; }
          closedir($dh); }
	  }
          echo "<td><a href=\"http://svnweb.cern.ch/trac/dirac/browser/LHCbDIRAC/tags/$pkg/$pkgver\">$pkgver</a></td>";
        }
      }
    } ?>
    </tr>
  </table>

  <br>
  <br>
  <br>
  </div>
  <?php include($scrpbase."/scripts/links.php"); ?>
  </body>
</html>

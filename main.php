<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
  <HEAD>
    <?php
    include(realpath(dirname(__FILE__))."/style.php");
    include "doxygen.inc";
    ?>

    <title><?php echo $projecttitle; ?></title>
    <meta http-equiv=Content-Language content=en-us>
    <meta http-equiv=Content-Type content="text/html">
  </HEAD>
    <BODY>

      <?php include($scrpbase."/scripts/title.php"); ?>

      <div class=pagebody>

      <?php
        if(file_exists($scrpbase."/manifest.html")) {
          include ($scrpbase."/manifest.html");
        } else {
          echo "<div ID=manifest >";
          echo "Welcome to the $projectname project website.";
          echo "</div>";
        }
       ?>

      <div id=ann>
        <H2>Announcements</H2>

        <?php
          $latestver =  end(explode('/', realpath("$scrpbase/releases/latest")));
        ?>

      <ul>
        <li>
          <span class=important>
            Latest current public release <a href="releases/<?php echo $latestver; ?>"> <?php echo $latestver; ?> </a>
          </span>

          <?php
            echo "(";
            if (file_exists("releases/$latestver/reldate.txt")) {
               echo trim(file_get_contents("releases/$latestver/reldate.txt"));
            } else {
               echo date("Y-m-d",filemtime(realpath("releases/$latestver/release.notes")));
            }
            echo "),";
          ?>

          with latest
          <b><a href="releases/<?php echo $latestver; ?>/release.notes.php">release notes</a></b><?php
           if(doxygen_exists($projectname, $latestver)) { ?>
            and latest
            <b><a href="<?php echo doxygen_url($projectname, $latestver); ?>">doxygen</a></b> documentation<?php
          }?>.
        </li>

        <?php
          $dir = "$scrpbase/releases";

          $bl = array();
          $bd = array();
          if(is_dir($dir)) {
            if($dh = opendir($dir)) {
              while(($file = readdir($dh)) != false) {
                if(!(array_search($file, array(".", "..")) > -1) && file_exists("$dir/$file/latest")) {
                  $latestver = end(explode('/', realpath("$dir/$file/latest")));

                  $bl[$file] = "";
                  $bl[$file] = $bl[$file]."<li>";
                  $bl[$file] = $bl[$file]."<span class=important>";
                  $bl[$file] = $bl[$file]."Latest $file compatible public release <a href=\"releases/$file/latest\"> $latestver </a>";
                  $bl[$file] = $bl[$file]."</span>";
                  $bl[$file] = $bl[$file]."(";
                  $bl[$file] = $bl[$file].date("Y-m-d",filemtime(realpath("releases/$latestver/release.notes")));
                  $bl[$file] = $bl[$file].")";
                  $bl[$file] = $bl[$file]."</li>";

                  $bd[$file] = date("Y-m-d",filemtime(realpath("releases/$latestver/release.notes")));

                }
              }
              closedir($dh);
            }
          }

          array_multisort($bd, SORT_DESC, $bl);
          foreach($bl as $text)
          {
            echo $text;
          }
        ?>
      </ul>

        <p>
          To have access to all the available versions, please
          follow this <a href="releases">link</a>.
        </p>
      </div>

      <?php
        if(file_exists($scrpbase."/maindesc.html")) {
          include ($scrpbase."/maindesc.html");
          echo "<br>";
        } ?>

    </div>

    <?php if(file_exists($scrpbase."/maindesc.html")) { include($scrpbase."/scripts/links.php"); } ?>

  </BODY>
</HTML>

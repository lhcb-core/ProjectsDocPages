<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <?php include(realpath(dirname(__FILE__))."/style.php"); ?>
    
    <title><?php echo $projectname ?> Packages</title>
  </head>

  <body>
  <?php include($scrpbase."/scripts/title.php") ?>
  <div class=pagebody>

  <h2>
  <?php
    echo "$projectname releases containing version $pkgver of package $pkgname"
  ?> 
  </h2>
  
  <?php
    $rellist = array();
    include(realpath(dirname(__FILE__))."/relist.php");
    foreach($releaselist as $rel)
    {
      if(file_exists($projectdirprefix.$rel."/".$_GET['pkgname']."/"."cmt"."/"."requirements"))
      {
        $rellist[] = $rel;
      } else {
	if(file_exists($projectdirprefix.$rel."/".$_GET['pkgname']."/".$_GET['pkgver']."/"."cmt"."/"."requirements"))
	  {
	    $rellist[] = $rel;
	  }
      }
    }
  ?>

  <?php
    $nrel = count($rellist);
    $ncol = ceil($nrel/10);

    echo "<table>";
    for($i = 0; $i < $ncol; $i++)
    {
      echo "<tr>";
      for($l = 0; $l < 10; $l++)
      {
        $m = $i + $l * $ncol;
        if($ncol == 1 && $m >= $nrel) break;
        $rel = $rellist[$m];

        echo "<td>";
          if(file_exists("$scrpbase/releases/$rel/index.php")) {
            echo "<a href=\"$project_base/releases/$rel\"><b>$rel</b></a>"; }
          else { echo "$rel"; }
        echo "</td>" ;
      } 
      echo "</tr>";
    }
    echo "</table>";
  ?>

  <?php 
  $rlnf = $projectdirprefix . $_GET['relver'] . "/" . $_GET['pkgname'] . "/doc/release.notes" ;
if (!file_exists($rlnf)) {
  $rlnf = $projectdirprefix . $_GET['relver'] . "/" . $_GET['pkgname'] . "/" . $_GET['pkgver'] . "/doc/release.notes" ;
 }
  if(file_exists($rlnf)) { ?>
  <h2>Release notes of <?php echo "" . $_GET['pkgname'] . " " . $_GET['pkgver']; ?></h2>
  <pre id=requ>
<?php
    $fl = file($rlnf);
#    $fl = file($scrpbase."/releases/".$_GET['relver']."/release.notes");
    $linenum1 = 0;
    $linenum2 = 0;
    foreach($fl as $linenum => $line)
    {
      if($linenum1 != 0 && $linenum2 == 0 && (strstr($line, "!=") || strstr($line, "! =") || strstr($line, "H1"))) $linenum2 = $linenum;
      if($linenum1 != 0 && $linenum2 == 0) echo $line;
      if($linenum1 == 0 && $linenum2 == 0 && (strstr($line, "!=") || strstr($line, "! =") || strstr($line, "H1")) && strstr($line, $_GET['pkgver'])) $linenum1 = $linenum;
    }
    if($linenum1 == 0 && $linenum2 == 0) foreach($fl as $linenum => $line) echo $line;
  ?>
  </pre>
  <?php } ?>
  <br>
  </div>
  <?php include($scrpbase."/scripts/links.php"); ?>
  </body>
</html>

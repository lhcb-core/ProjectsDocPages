<?php

  $pkgdir = "$scrpbase/packages";
  $ignoredDirectory[] = ".";
  $ignoredDirectory[] = "..";
  $ignoredDirectory[] = "index.php";
  $ignoredDirectory[] = "package.php";

  $pkglist = array();
  $verlist = array();
  $i = 0;
  $j = 0;
  $pkglist[$i] = "$projectname";
  $verlist[$i] = "$projectversion";
  $i++;

  if(is_dir($pkgdir) && $dh = opendir($pkgdir)) { while(($file = readdir($dh)) !== false)
  {
    $j = 0;
    if(!(array_search($file,$ignoredDirectory) > -1))
    {
      if(is_dir($pkgdir . "/" . $file) && $dh1 = opendir($pkgdir . "/" . $file)) { while(($file1 = readdir($dh1)) !== false)
      {
        if(!(array_search($file1,$ignoredDirectory) > -1))
        {
          $pkglist[$i] = $file . "/" . $file1;
          $i++;
          $j++;
        }
      }
      closedir($dh1); }
      if($j == 0)
      {
        $pkglist[$i] = $file;
        $i++;
      }
    }
  }
  closedir($dh); }

?>

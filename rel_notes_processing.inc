<?php
require 'php-markdown/Michelf/MarkdownExtra.inc.php';
use Michelf\MarkdownExtra;

function urls_to_links($text) {
  return preg_replace('|(?<![\[\(])https?://[-a-zA-Z0-9./]+|', '[$0]($0)', $text);
}
function convert_gilab_mr_links($text) {
  return preg_replace_callback('|([-a-zA-Z0-9_]+/[-a-zA-Z0-9_]+)?!([0-9]+)|',
    function ($matches) {
      global $project_repo;
      if ($matches[1]) {
        $repo = $matches[1];
      } else {
        $repo = $project_repo;
      }
      return "[$matches[0]](https://gitlab.cern.ch/$repo/merge_requests/$matches[2])";
    },
    $text
  );
}
function convert_jira_issue_links($text) {
  return preg_replace('/\b[A-Z][A-Z0-9]*-[0-9]+/', '[$0](https://its.cern.ch/jira/browse/$0)', $text);
}

function format_rel_notes($filename) {
  $text = file_get_contents($filename);
  $text = urls_to_links($text);
  $text = convert_gilab_mr_links($text);
  $text = convert_jira_issue_links($text);
  if (preg_match('|.*/(v[0-9]+.*)\.md|', $filename, $matches)) {
    $anchor = '<a name="' . $matches[1] . '"></a>';
  }
  return $anchor . MarkdownExtra::defaultTransform($text);
}
?>

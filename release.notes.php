<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <?php include(realpath(dirname(__FILE__))."/style.php"); ?>

    <title><?php echo $projectname; ?> Release Notes</title>
    <meta http-equiv=Content-Language content=en-us>
    <meta http-equiv=Content-Type content="text/html">
  </head>

  <body>
    <?php include($scrpbase."/scripts/title.php"); ?>
    <div class=pagebody>
      <?php include("release.notes.html"); ?>
      <br>
      <br>
      <br>
    </div>
    <?php include($scrpbase."/scripts/links.php"); ?>

  </body>
</html>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <?php
    include(realpath(dirname(__FILE__))."/style.php");
    include "doxygen.inc";
    include "rel_notes_processing.inc";
    ?>

    <?php
      $dir = getcwd();
      $part = explode('/', $dir);
    ?>
    <title><?php echo $projectname . " " . end($part); ?> Release Notes </title>
  </head>

  <body>
    <?php include($scrpbase."/scripts/title.php"); ?>
    <?php include($scrpbase."/scripts/release_title.php"); ?>
    <div class=pagebody>
    <h1> The <?php echo $projectname . " " . end($part); ?> Full Release Notes </h1>

    <?php
      if (file_exists('ReleaseNotes')) {
        $chunks = scandir('ReleaseNotes');
        natsort($chunks);
        $chunks = array_reverse($chunks);
        foreach ($chunks as $chunk) {
          if (preg_match("/v[0-9].*\\.md/", $chunk)) {
            echo format_rel_notes('ReleaseNotes/' . $chunk);
          }
        }
      }

      if (file_exists('release.notes')) {
        if (file_exists('ReleaseNotes'))
          echo '<br/><br/><hr/><div style="text-align: center;"><b>=== old style release notes ===</b></div><hr/>';
        echo "<pre>";
        $fl = file("release.notes");
        foreach($fl as $line)
        {
	  //          echo ereg_replace("<[^>]+>[ ]*", "", $line);
          echo $line;
        }
        echo "</pre>";
      }
    ?>

    <br>
    <br>
    <br>

    </div>
    <?php include($scrpbase."/scripts/release_links.php"); ?>
    <?php include($scrpbase."/scripts/links.php"); ?>
  </body>
</html>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <?php
    include(realpath(dirname(__FILE__))."/style.php");
    include "doxygen.inc";
    include "rel_notes_processing.inc";
    ?>

    <title>
      <?php
        $dir = getcwd();
        $releasever = end(explode('/', $dir));
        echo $projectname . " " . $releasever;
      ?>
    </title>
  </head>

  <body>
    <?php include($scrpbase."/scripts/title.php"); ?>
    <?php include($scrpbase."/scripts/release_title.php"); ?>

    <div class=pagebody>

    <?php
      if(file_exists("description.html")) {
        $fl = file("description.html");
        echo "<h2>Description</h2>";
        foreach($fl as $linenum => $line) echo $line;
      } else if(file_exists("description.php")) {
        echo "<h2>Description</h2>";
        include("description.php");
      }
    ?>

    <?php
      if (file_exists('ReleaseNotes/' . $releasever . '.md')) {
        echo "<h2>Release Notes</h2>\n<div id=\"requ\">";
        echo format_rel_notes('ReleaseNotes/' . $releasever . '.md');
        echo "</div>\n";
      } else if (file_exists('release.notes')) {
        echo "<h2>Release Notes </h2>";
        $fl = file("release.notes");
        $linenum1 = 0;
        $linenum2 = 0;
        echo "<div id=requ><p>";
        //echo "<pre>";
        foreach($fl as $linenum => $line)
        {
          if($linenum1 != 0 && $linenum2 == 0 && (strstr($line, "<H1>") || strstr($line, "!="))) $linenum2 = $linenum;
          if($linenum1 != 0 && $linenum2 == 0) {
            $regs = array();
            if(ereg("^[ ]+", $line, $regs))
            {
              $cnt = strlen($regs[0]);
              for($i = 0; $i < $cnt; $i++) echo "&nbsp;";
            }

            echo ereg_replace("<[^>]+>[ ]*", "", ereg_replace("^[ ]+", "", $line))."<br>";
          }
          if($linenum1 == 0 && $linenum2 == 0 && (strstr($line, "<H1>") || strstr($line, "!=")) && strstr($line, $releasever)) $linenum1 = $linenum;
        }
        if($linenum1 == 0 && $linenum2 == 0) { foreach($fl as $line) { echo $line; } }
        //echo "</pre>";
        echo "</p></div>";
      }
    ?>


    <h2>Package Versions</h2>

    <?php
    include($scrpbase."/scripts/pkglist.php");

    $pkgver = array();
    $pkglist1 = array();
    foreach($pkglist as $pkg)
    {
//      if($pkg == $projectname) { $pkgver[] = ""; $pkglist1[] = $pkg; }
      $pkgdir = $projectdirprefix . $releasever . "/" . $pkg;
      $pkgvercmtf = $projectdirprefix . $releasever . "/" . $pkg . "/cmt/version.cmt" ;
      if(file_exists($pkgvercmtf)){
	  $pkgver[] = implode('',file($pkgvercmtf)) ;
	  $pkglist1[] = $pkg;
      } else {
      if(is_dir($pkgdir) && $dh = opendir($pkgdir)) { while(($file = readdir($dh)) !== false) {
        if(!(array_search($file,$ignoredDirectory) > -1)) { $pkgver[] = $file; $pkglist1[] = $pkg; } }
      closedir($dh); }
      }
    }

    $pkglist = $pkglist1;
    $nbofpkg = count($pkglist);
    $nbpkgcol = ceil($nbofpkg/3);

    echo "<table>";
    for($i = 0; $i < $nbpkgcol; $i++)
    {
      echo "<tr>";
      for($l = 0; $l < 3; $l++)
      {
        $m = $i + $l * $nbpkgcol;
        if($nbpkgcol == 1 && $m >= $nbofpkg) break;
        $pkg = $pkglist[$m];

        echo "<td class=firstcell>";
          if(file_exists("$scrpbase/packages/$pkg/index.php")) {
            echo "<a href=\"$project_base/packages/$pkg\">$pkg</a>"; }
          else { echo "$pkg"; }
        echo "</td>" ;

        echo "<td>";
          if($pkg == $projectname) {
            echo "<a href=\".\">$releasever</a>"; }
          else if($pkgver[$m] != "") echo "<a href=\"$project_base/packages/package.php?relver=" . $releasever . "&pkgname=" . $pkg . "&pkgver=" . $pkgver[$m] . "\">" . $pkgver[$m] . "</a>";
        echo "</td>";
      }
      echo "</tr>";
    }
    echo "</table>";
  ?>

  <br>

  </div>

  <?php include($scrpbase."/scripts/release_links.php"); ?>
  <?php include($scrpbase."/scripts/links.php"); ?>

  </body>
</html>

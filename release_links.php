
  <table id=relinks>
    <tbody>
    <tr>

    <?php
    if (file_exists('release.notes')) {
      echo "<td>";
      echo '<a href="release.notes.php">The Full Release Notes</a>';
      echo "</td>";
    }
    if (doxygen_exists($projectname, $projectversion)) {
      echo "<td>";
      echo '<a href="' . doxygen_url($projectname, $projectversion) . '">The Doxygen Documentation</a>';
      echo "</td>";
    }

    if (file_exists('coverage')) {
      echo "<td>";
      echo '<a href="coverage">The Code Coverage</a> ';
      echo "</td>";
    }

    if (file_exists('requirements')) {
      echo "<td>";
      echo '<a href="requirements.php">The Requirements File</a> ';
      echo "</td>";
    }
    ?>

    </tr>
    </tbody>
  </table>

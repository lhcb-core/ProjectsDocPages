<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <?php
    include(realpath(dirname(__FILE__))."/style.php");
    include "doxygen.inc";
    ?>

    <title><?php echo $projectname ?> Releases</title>
  </head>

  <body>

  <?php include($scrpbase."/scripts/title.php"); ?>

  <div class=pagebody>

  <?php
    echo "<h2>$projectname Releases</h2>";
    echo "<p> Here is a summary of the different versions of
    $projectname available. You can click on the links in the first
    column to access each individual release.</p>";

    include($scrpbase."/scripts/relist.php"); ?>

    <table>
      <tr>
        <td id="latest">Latest version:</td><td>Latest version. Users are encouraged to migrate to this.</td>
      </tr>
      <tr>
        <td id="production">Production versions:</td><td>Production versions still in use</td>
      </tr>
      <tr>
        <td id="obsolete">Obsolete versions:</td><td>Versions that will be removed.</td>
      </tr>
    </table>

    <br>

    <table>
    <?php
      foreach($releaselist_back as $rel) if(file_exists($rel)) {
        echo '<tr>';
        echo '<td class=firstcell>' ;
        echo "<a href=\"$rel\"> $rel </a>" ;
        echo '</td>';

        echo '<td align="center">';
        if($branchlist[$rel] == "") { echo "DEV"; }
        else { echo "<a href=\"$project_base/releases/$branchlist[$rel]\">$branchlist[$rel]</a>"; }
        echo '</td>';

        echo '<td align="center">' ;
        if (file_exists("$rel/ReleaseNotes/" . $rel . ".md")) {
          echo date("Y-m-d",filemtime(realpath("$rel/ReleaseNotes/" . $rel . ".md"))); }
        else if (file_exists("$rel/release.notes")) {
          echo date("Y-m-d",filemtime(realpath("$rel/release.notes"))); }
        else { echo "NA"; }
        echo '</td>';

        echo '<td align="center">' ;
        if (file_exists("$rel/ReleaseNotes/" . $rel . ".md") || file_exists("$rel/release.notes")) {
          echo "<a href=\"$rel/release.notes.php\">Release Notes</a>"; }
        else { echo "NA"; }
        echo '</td>';

        echo '<td align="center">' ;
        if (doxygen_exists($projectname, $rel)) {
          echo "<a href=\"" . doxygen_url($projectname, $rel) . "\">Doxygen</a>"; }
        else { echo "NA"; }
        echo '</td>';

        $descfile = $scrpbase . "/releases/" . $rel . "/description.html";
        if(file_exists($descfile)) {
          $fl = file($descfile);
          foreach($fl as $linenum => $line) echo $line; }
        else if (file_exists(str_replace(".html", ".php", $descfile))) {
          include(str_replace(".html", ".php", $descfile));
        } else { echo "<td align=\"center\" width=\"50%\">N/A</td>"; }

        echo '</tr>';
      }
    ?>
    </table>

  <br><br><br>

  </div>

  <?php include($scrpbase."/scripts/links.php"); ?>

  </body>
</html>

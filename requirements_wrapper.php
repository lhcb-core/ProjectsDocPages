<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <?php
    include(realpath(dirname(__FILE__))."/style.php");
    include "doxygen.inc";
    ?>

    <?php
      $dir = getcwd();
      $part = explode('/', $dir);
    ?>
    <title><?php echo $projectname . " " . end($part); ?>  Requirements</title>
  </head>

  <body>
    <?php include($scrpbase."/scripts/title.php"); ?>
    <?php include($scrpbase."/scripts/release_title.php"); ?>

    <div class=pagebody>
    <h1 align="center"><?php echo $projectname . " " . end($part); ?>  Requirements</h1>
    <?php
      if (file_exists('requirements')) {
        echo '<pre id=requ>';
        include 'requirements';
        echo '</pre>';
      } ?>
    </div>

    <?php include($scrpbase."/scripts/release_links.php"); ?>
    <?php include($scrpbase."/scripts/links.php"); ?>

  </body>
</html>

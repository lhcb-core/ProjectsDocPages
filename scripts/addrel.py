#!/usr/bin/env python

from __future__ import print_function
import sys
import os
import re
import shutil
import time

from stat import *

if len(sys.argv) < 3:
    print("usage:")
#  print "  projectname projectversion [lhcbsitelocation]"
    print("  projectname projectversion [branch]")
    exit(1)

projectname = sys.argv[1]
projectversion = sys.argv[2]
lhcbsitelocation = "/eos/project/l/lhcbwebsites/www/projects"
branch = ""
if len(sys.argv) > 3:
#  lhcbsitelocation = sys.argv[3]
    branch = sys.argv[3]

if lhcbsitelocation != "" and lhcbsitelocation.endswith("/") == 0:
    lhcbsitelocation = lhcbsitelocation + "/"
else:
    lhcbsitelocation = lhcbsitelocation
lhcbsitelocation = lhcbsitelocation + projectname.lower() + "/"

version_regex = re.compile("v([0-9]+)(?:r([0-9]+)(?:p([0-9]+))?)?")

print()
print(" -------------------------- Project " + projectname.lower() + " " + projectversion.lower() + " --------------------------")

# obtain path to project location and checking for errors (existing of the project, version and SysPackage)

projectdir = '/cvmfs/lhcb.cern.ch/lib/lhcb/'
syspkgdir = ""
projectroot = ""
relprojectdir = ""
relsyspkgdir = ""
error = "Project " + projectname.upper() + " does not exist!"
for dir in os.listdir(projectdir):
    if dir.isupper() and dir.lower() == projectname.lower():
        projectdir = projectdir + dir + "/"
        projectroot = projectdir
        error = "Project version " + projectversion + " does not exist!"
for dir in os.listdir(projectdir):
    if dir.lower() == (projectname + "_" + projectversion).lower():
        projectdir = projectdir + dir + "/"
        relprojectdir = dir + "/"
        relsyspkgdir = dir + "/"
        error = "System package missing!"
for dir in os.listdir(projectdir):
    if dir.lower() == (projectname + "sys").lower():
        syspkgdir = projectdir + dir + "/"
        relsyspkgdir = relsyspkgdir + dir + "/"
        if not os.path.exists(os.path.join(syspkgdir, "cmt")) and not os.path.exists(os.path.join(syspkgdir, "CMakeLists.txt")) :
            syspkgdir = projectdir + dir + "/" + projectversion + "/"
            relsyspkgdir = relsyspkgdir + dir + "/" + projectversion + "/"
        error = ""
if error != "":
    print("ERROR:", error)
    raise SystemExit

# remove all files in releases/[version] except file description.html
# MCl: keep more files in release/version (see LBCORE-1091)
try:
    for file in os.listdir(lhcbsitelocation + "releases/" + projectversion):
        if file not in ("description.html", "description.txt", "description.php", "TCKs.txt"):
            os.remove(lhcbsitelocation + "releases/" + projectversion + "/" + file)
except os.error as err:
    err

# recreate projectroot link

try:
    os.remove(lhcbsitelocation + "projectroot")
except os.error as err:
    err
try:
    os.symlink(projectroot, lhcbsitelocation + "projectroot")
except os.error as err:
    err

# create links in releases/[version]

print("LINKS:")
try:
    os.mkdir(lhcbsitelocation + "releases")
    os.symlink("../scripts/releases.php", lhcbsitelocation + "releases/index.php")
except os.error as err:
    err

try:
    try:
        os.mkdir(lhcbsitelocation + "releases/" + projectversion)
    except os.error as err:
        err
    os.symlink("../../scripts/release.php", lhcbsitelocation + "releases/" + projectversion + "/index.php")
    print("  index.php -> " + "../../scripts/release.php")
    os.symlink("../../scripts/release.notes_wrapper.php", lhcbsitelocation + "releases/" + projectversion + "/release.notes.php")
    print("  release.notes.php -> " + "../../scripts/release.notes_wrapper.php")
    os.symlink("../../scripts/requirements_wrapper.php", lhcbsitelocation + "releases/" + projectversion + "/requirements.php")
    print("  requirements.php -> " + "../../scripts/requirements_wrapper.php")
    if os.path.exists(os.path.join(lhcbsitelocation, 'moore_description.php')):
        os.symlink("../../moore_description.php", os.path.join(lhcbsitelocation, 'releases', projectversion, 'description.php'))
        print("  description.php -> ../../moore_description.php")
except os.error as err:
    err

try:
    os.symlink("../../projectroot/" + relsyspkgdir + "cmt/requirements", lhcbsitelocation + "releases/" + projectversion + "/requirements")
    print("  requirements -> " + "../../projectroot/" + relsyspkgdir + "cmt/requirements")
    src_rel_notes = os.path.join(projectroot, relprojectdir, 'ReleaseNotes')
    dst_rel_notes = os.path.join(lhcbsitelocation, 'releases', projectversion, 'ReleaseNotes')
    if os.path.isdir(src_rel_notes):
        if os.path.exists(dst_rel_notes):
            try:
                shutil.rmtree(dst_rel_notes + '.bk')
            except:
                pass
            rename(dst_rel_notes, dst_rel_notes + '.bk')
        shutil.copytree(src_rel_notes, dst_rel_notes)
        print("  ReleaseNotes -> ../../projectroot/%s/ReleaseNotes" % relprojectdir)
    for file in os.listdir(syspkgdir + "doc"):
        if file.find("release") != -1 and file.find("notes") != -1:
            os.symlink("../../projectroot/" + relsyspkgdir + "doc/" + file, lhcbsitelocation + "releases/" + projectversion + "/release.notes")
            print("  release.notes -> " + "../../projectroot/" + relsyspkgdir + "doc/" + file)
            break
    if os.path.isfile(projectdir + "description.html"):
        os.symlink("../../projectroot/" + relprojectdir + "description.html", lhcbsitelocation + "releases/" + projectversion + "/description.html")
        print("  description.html -> ../../projectroot/" + relprojectdir + "description.html")
    if not os.path.exists(os.path.join(lhcbsitelocation,"releases/", projectversion, "reldate.txt")):
        relDate = time.strftime("%Y-%m-%d",time.localtime())
        dateFile = open(os.path.join(lhcbsitelocation,"releases/", projectversion,"reldate.txt"),'w')
        dateFile.write(relDate+'\n')
        dateFile.close()
except os.error as err:
    err

try:
    for file in os.listdir(syspkgdir + "doc"):
        if file.find("release") != -1 and file.find("notes") != -1:
            try:
                os.symlink("../../projectroot/" + relsyspkgdir + "doc/" + file, lhcbsitelocation + "releases/" + projectversion + "/" + file)
                print("  " + file + " -> " + "../../projectroot/" + relsyspkgdir + "doc/" + file)
            except os.error as err:
                err
except os.error as err:
    err

# create packages structure for current version of project

print("PACKAGES:")
try:
    os.mkdir(lhcbsitelocation + "packages")
    os.symlink("../scripts/packages.php", lhcbsitelocation + "packages/index.php")
    os.symlink("../scripts/package.php", lhcbsitelocation + "packages/package.php")
except os.error as err:
    err
for pkg1 in os.listdir(projectdir):
    if S_ISDIR(os.stat(projectdir + pkg1)[ST_MODE]):
        for pkg2 in os.listdir(projectdir + pkg1):
            if (version_regex.match(pkg2) != None or os.path.isfile(projectdir + pkg1 + "/cmt/version.cmt")) and pkg1.lower() != (projectname + "sys").lower():
                try:
                    os.mkdir(lhcbsitelocation + "packages/" + pkg1);
                except os.error as err:
                    err
                print("  " + pkg1)
            else:
                if S_ISDIR(os.stat(projectdir + pkg1 + "/" + pkg2)[ST_MODE]):
                    pkg2listdir = os.listdir(projectdir + pkg1 + "/" + pkg2)
                    if len(pkg2listdir) > 0:
                        if version_regex.match(pkg2listdir[0]) != None or os.path.isfile( projectdir + pkg1 + "/" + pkg2 + "/cmt/version.cmt" ):
                            try:
                                os.mkdir(lhcbsitelocation + "packages/" + pkg1)
                            except os.error as err:
                                err
                            try:
                                os.mkdir(lhcbsitelocation + "packages/" + pkg1 + "/" + pkg2)
                            except os.error as err:
                                err
                            print("  " + pkg1 + "/" + pkg2)

# create latest link in releases directory

try:
    os.remove(lhcbsitelocation + "releases/latest")
except os.error as err:
    err

versions = []
for dir in os.listdir(lhcbsitelocation + "releases/"):
    m = version_regex.match(dir)
    if m != None:
        v = [int(g) if g is not None else 0 for g in m.groups()]
        v.append(dir)
        versions.append(v)

versions.sort()
try:
    os.remove(lhcbsitelocation + "releases/latest")
except os.error as err:
    err
os.symlink(versions[-1][3], lhcbsitelocation + "releases/latest")

# if branch is specified create latest link in releases/[branch] directory

if branch != "":
    try:
        os.mkdir(lhcbsitelocation + "releases/" + branch)
        os.symlink("../../scripts/releases.php", lhcbsitelocation + "releases/" + branch + "/index.php")
    except os.error as err:
        err
    try:
        os.symlink("../" + projectversion, lhcbsitelocation + "releases/" + branch + "/" + projectversion)
    except os.error as err:
        err

    versions = []
    for dir in os.listdir(lhcbsitelocation + "releases/" + branch):
        m = version_regex.match(dir)
        if m != None:
            v = [int(g) if g is not None else 0 for g in m.groups()]
            v.append(dir)
            versions.append(v)

    versions.sort()
    try:
        os.remove(lhcbsitelocation + "releases/" + branch + "/latest")
    except os.error as err:
        err
    os.symlink(versions[-1][3], lhcbsitelocation + "releases/" + branch + "/latest")

print(" -------------------------- Release " + projectversion + " added. ---------------------------")
print()

from __future__ import print_function
import sys, os, time, stat, shutil

if len(sys.argv) != 3:
    message = 'Provide as argument application and version to archive'
    raise RuntimeError(message)

project = sys.argv[1]
version = sys.argv[2]

print('Making archive documentation for', project, version)

docDir = os.path.join(os.environ['LHCBDOC'],project,'releases')
relDir = os.path.join(docDir,version)
arcDir = os.path.join(docDir,'archive',version)
print(docDir)
print(relDir)
print(arcDir)

arcVers = os.listdir(os.path.join(docDir,'archive'))
if version in arcVers:
    message = 'Archive directory already exists '+arcDir+' - Check it - STOP'
    raise Exception(message)

message = 'Create archive directory '+arcDir
print(message)
os.mkdir(arcDir)

print('Copying description file')
shutil.copyfile(os.path.join(relDir,'description.html'),os.path.join(arcDir,'description.html'))
shutil.copyfile(os.path.join(relDir,'reldate.txt'),os.path.join(arcDir,'reldate.txt'))

arcDate = time.strftime("%Y-%m-%d",time.localtime())
dateFile = file(os.path.join(arcDir,'arcdate.txt'),'w')
dateFile.write(arcDate+'\n')
dateFile.close()


#!/usr/bin/python

from __future__ import print_function
import shutil
import sys
import os
import re
from stat import *

version_regex = re.compile("v([0-9]+)(?:r([0-9]+)(?:p([0-9]+))?)?")

if len(sys.argv) < 3:
    print("usage:")
#  print "  projectname projectversion [lhcbsitelocation]"
    print("  projectname projectversion [branch]")
    exit(1)

projectname = sys.argv[1]
projectversion = sys.argv[2]
lhcbsitelocation = ""
branch = ""
if len(sys.argv) > 3:
#  lhcbsitelocation = sys.argv[3]
    branch = sys.argv[3]
if lhcbsitelocation != "" and lhcbsitelocation.endswith("/") == 0:
    lhcbsitelocation = lhcbsitelocation + "/"
else:
    lhcbsitelocation = lhcbsitelocation;
lhcbsitelocation = lhcbsitelocation + projectname.lower() + "/"

print()
print(" -- Project " + projectname.lower() + " " + projectversion.lower() + "--")

# recreate latest links

# try to find branch for this version and remove it from branch and recreate latest link

for dir in os.listdir(lhcbsitelocation + "releases"):
    if version_regex.match(dir) == None:
        try:
            ln = len(os.listdir(lhcbsitelocation + "releases/" + dir))
            try:
                os.remove(lhcbsitelocation + "releases/" + dir + "/" + projectversion)
                if ln <= 3:
                    shutil.rmtree(lhcbsitelocation + "releases/" + dir)
                else:
                    try:
                        os.remove(lhcbsitelocation + "releases/" + dir + "/latest")
                    except os.error as err:
                        err

                    versions = []
                    for dirv in os.listdir(lhcbsitelocation + "releases/" + dir):
                        m = version_regex.match(dirv)
                        if m != None:
                            v = []
                            if m.group(1) != None:
                                v.append(int(m.group(1)))
                            else:
                                v.append(m.group(1))
                            if m.group(2) != None:
                                v.append(int(m.group(2)))
                            else:
                                v.append(m.group(2))
                            if m.group(3) != None:
                                v.append(int(m.group(3)))
                            else:
                                v.append(m.group(3))
                            v.append(dirv)
                            versions.append(v)

                    versions.sort()
                    os.symlink(versions[-1][3], lhcbsitelocation + "releases/" + dir + "/latest")
            except os.error as err:
                err
        except os.error as err:
            err

# if remove from branch - exit

if branch != "":
    print(" :: Release " + projectversion + " removed from branch " + branch + ".")
    print()
    raise SystemExit

# if no - remove version and recreate latest link

shutil.rmtree(lhcbsitelocation + "releases/" + projectversion)

try:
    os.remove(lhcbsitelocation + "releases/latest")
except os.error as err:
    err

versions = []
for dir in os.listdir(lhcbsitelocation + "releases/"):
    m = version_regex.match(dir)
    if m != None:
        v = []
        if m.group(1) != None:
            v.append(int(m.group(1)))
        else:
            v.append(m.group(1))
        if m.group(2) != None:
            v.append(int(m.group(2)))
        else:
            v.append(m.group(2))
        if m.group(3) != None:
            v.append(int(m.group(3)))
        else:
            v.append(m.group(3))
        v.append(dir)
        versions.append(v)

versions.sort()
os.symlink(versions[-1][3], lhcbsitelocation + "releases/latest")

print(" :: Release " + projectversion + " removed.")
print()

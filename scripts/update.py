#!/usr/bin/python

import sys
import os
import re
from stat import *

version_regex = re.compile("v([0-9]+)(?:r([0-9]+)(?:p([0-9]+))?)?")

for project in os.listdir("."):
    try:
        for ver in os.listdir(project + "/releases"):
            if version_regex.match(ver) != None:
                os.system("./addrel.py " + project + " " + ver)
    except os.error as err:
        err

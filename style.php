<?php

  $lhcbreleases = "/cvmfs/lhcb.cern.ch/lib/lhcb/";
  $site_base = "https://lhcbdoc.web.cern.ch/lhcbdoc/";

  $projectname = strtoupper(current(explode('/', str_replace($site_base, "", "https://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']))));

  if ($projectname == 'LHCB') {
    $project_canonical_name = 'LHCb';
  } elseif ($projectname == 'DAVINCI') {
    $project_canonical_name = 'DaVinci';
  } elseif ($projectname == 'ALIGNMENTONLINE') {
    $project_canonical_name = 'AlignmentOnline';
  } elseif ($projectname == 'MOOREONLINE') {
    $project_canonical_name = 'MooreOnline';
  } else {
    $project_canonical_name = ucfirst(strtolower($projectname));
  }
  $project_repo = 'lhcb/' . $project_canonical_name;

  $scrpbase = realpath(dirname(__FILE__)."/..")."/".strtolower($projectname);

  $project_base = $site_base.strtolower($projectname);
  $projectdirprefix = $lhcbreleases.strtoupper($projectname)."/".strtoupper($projectname)."_";

  echo "<link rel=\"icon\" type=\"image/jpg\" href=\"$site_base"."images/lhcblogo.gif\">\n";
  echo "<link rel=\"stylesheet\" href=\"$site_base"."css/lhcb.css\" type=\"text/css\" media=\"screen\">\n";
  if(file_exists($scrpbase."/css/css.css")) {
    echo "<link rel=\"stylesheet\" href=\"$project_base/css/css.css\" type=\"text/css\" media=\"screen\">\n";
  }

  if(0 && file_exists($scrpbase."/maintitle.html")) {
    ob_start();
    include($scrpbase."/maintitle.html");
    $projecttitle = ob_get_contents();
    ob_end_clean();
  } else {
    $projecttitle = "The $projectname Project" ;
  }


?>

<div class="ctitle">

<!--  <?php include(realpath(dirname(__FILE__))."/style.php"); ?> -->

  <TABLE id="pagetitle">
    <TBODY>
      <TR>
        <TD class=iconspace>
          <A href="https://cern.ch/lhcb-comp">
          <IMG id=lhcblogo <?php echo " src=\"$site_base"."images/lhcbcomputinglogo.gif\""; ?> >
          </A>
        </TD>
        <TD vAlign=middle align=center>
        <?php
         if(file_exists($scrpbase."/maintitle.html"))
         {
           echo "<H1><a href=\"$project_base\" >\n";
           include($scrpbase."/maintitle.html");
           echo "</a></H1>\n";
         }
         else
         {
           echo "<H1><a href=\"$project_base\" >The $projectname Project</a></H1>";
         }
        ?></TD>
      </TR>
    </TBODY>
  </TABLE>

  <?php include($scrpbase."/scripts/links.php"); ?>

</div>

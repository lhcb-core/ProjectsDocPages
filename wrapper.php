<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
  <HEAD>
    <?php include(realpath(dirname(__FILE__))."/style.php"); ?>
    
    <title>The <?php echo $projectname; ?> Framework</title>
    <meta http-equiv=Content-Language content=en-us>
    <meta http-equiv=Content-Type content="text/html">
  </HEAD>
    <BODY>

      <?php include($scrpbase."/scripts/title.php"); 
      $wrappedfile = basename($_SERVER['PHP_SELF'],".php");
      if (strcmp($wrappedfile,"index") == 0 ) $wrappedfile = "main"
      ?>
      <div class=pagebody>
	<?php /*>*/
	include($wrappedfile.".html");
	?>
      </div>


    </div>
    
    <?php if(file_exists($scrpbase."/maindesc.html")) { include($scrpbase."/scripts/links.php"); } ?>
    
  </BODY>
</HTML>
